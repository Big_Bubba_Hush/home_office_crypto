package service;

import model.Currency;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import repository.RestServiceInterface;

import java.util.Arrays;

@Service
public class CurrencyService implements RestServiceInterface {

    @Override
    public Currency findById(String id){

        RestTemplate restTemplate = new RestTemplate();
        Currency[] currencyList =
                restTemplate.getForObject("https://api.nomics.com/v1/currencies/ticker?key=demo-26240835858194712a4f8cc0dc635c7a&ids="+id.toUpperCase()+"&interval=1d,30d&convert=USD", Currency[].class);


        return currencyList[0];
    }

    @Override
    public Currency[] findTop10(){

        RestTemplate restTemplate = new RestTemplate();
        Currency[] currencyList =
                restTemplate.getForObject("https://api.nomics.com/v1/currencies/ticker?key=demo-26240835858194712a4f8cc0dc635c7a&interval=1d,30d&convert=USD", Currency[].class);


        Currency[] top10Crypto = Arrays.copyOf(currencyList, 10);

        return top10Crypto;
    }

}
