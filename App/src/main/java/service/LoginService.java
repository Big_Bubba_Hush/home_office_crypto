package service;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.UserRepositoryInterface;

import javax.sql.DataSource;
import java.util.List;

@Service
public class LoginService {

    @Autowired
    DataSource dataSource;

    @Autowired
    UserRepositoryInterface personRepo;

    public User authenticate(String username, String password) {

        User users = personRepo.findByUsername(username);

        if(users != null ) {
            if (users.getPassword().equals(password)){
                return users;
            }
        }
        return users;
    }
}
