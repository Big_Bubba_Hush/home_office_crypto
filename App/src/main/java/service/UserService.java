package service;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.UserRepositoryInterface;
import repository.UserUpdateInterface;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    Hashtable<String, User> persons = new Hashtable<String, User>();

    @Autowired
    DataSource dataSource;

    @Autowired
    UserRepositoryInterface userRepo;

    @Autowired
    UserUpdateInterface userUpdate;

    public Optional<User> getUser(long id){
        return userRepo.findById(id);
    }

    public List<User> getAll(){
       return (ArrayList<User>)userRepo.findAll();
    }

    public List<User> findByName(String first_name) {
        return userRepo.findByFirstName(first_name);
    }

    public String addUser(int age, String first_name, String last_name, String email, String level, String password, String username){
        User user = new User();

        user.setAge(age);
        user.setFirstName(first_name);
        user.setLastName(last_name);
        user.setEmail(email);
        user.setLevel(level);
        user.setPassword(password);
        user.setUsername(username);

        User emailResult = userRepo.findByEmail(email);

        if(emailResult == null ){
            User usernameResult = userRepo.findByUsername(username);
           if(usernameResult == null) {
               userRepo.save(user);
               return "Ok";
           }
        }

        return "Error creating user";
    }

    public void deleteUser(long id) {
        userRepo.deleteById(id);
    }

    @Transactional
    public void modifyUser(String first_name, long id) {
        userUpdate.updateUserName(first_name,id);
    }


}
