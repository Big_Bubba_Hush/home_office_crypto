package model;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Currency {
    String name;
    float price;
    int rank;
    long circulatingSupply;
    long maxSupply;
    String logo;


    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public long getCirculatingSupply() {
        return circulatingSupply;
    }

    public void setCirculating_supply(long circulating_supply) {
        this.circulatingSupply = circulating_supply;
    }

    public long getMaxSupply() {
        return maxSupply;
    }

    public void setMax_supply(long max_supply) {
        this.maxSupply = max_supply;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo_url(String logo_url) {
        this.logo = logo_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String currency) {
        this.name = currency;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
