package model;

public class CurrencyChange {
    long volume;
    float priceChange;
    long volumeChange;

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public float getPriceChange() {
        return priceChange;
    }

    public void setPrice_change(float priceChange) {
        this.priceChange = priceChange;
    }

    public long getVolumeChange() {
        return volumeChange;
    }

    public void setVolume_change(long volumeChange) {
        this.volumeChange = volumeChange;
    }
}
