package client;

import model.Currency;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CallRestService implements CommandLineRunner {

    private static void CallRestService(){
        RestTemplate restTemplate = new RestTemplate();
        Currency[] currencyList =
                restTemplate.getForObject("https://api.nomics.com/v1/currencies/ticker?key=220f9c5c01b20545573e83d34dec4ed5&ids=BTC,ETH&interval=1d,30d&convert=USD%22", Currency[].class);


        System.out.println(currencyList[1].getName());
    }

    @Override
    public void run(String... args) throws Exception {
        CallRestService();
    }
}
