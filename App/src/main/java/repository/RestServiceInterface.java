package repository;

import model.Currency;

public interface RestServiceInterface {

    Currency findById(String id);

    Currency[] findTop10();
}
