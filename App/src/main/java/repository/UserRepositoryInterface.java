package repository;

import model.User;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepositoryInterface extends CrudRepository<User, Long>{

    List<User> findByAge(long Age);

    List<User> findByFirstName(String firstName);

    User findByUsername(String username);

    User findByEmail(String email);

}

