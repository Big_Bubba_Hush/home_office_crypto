package repository;

import model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserUpdateInterface extends JpaRepository<User, Long> {
    @Modifying
    @Query(value="update User u set u.first_name = ?1 where u.id = ?2", nativeQuery = true)
    void updateUserName(@Param("first_name") String name, @Param("id") long id);

}
