package controller;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.UserService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    UserService ps;

    @GetMapping("/all")
    public List<User> getAll(){
        return ps.getAll();
    }

    @GetMapping("{id}")
    public Optional<User> getUser(@PathVariable("id") int id){
        return ps.getUser(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public String addPerson(@RequestBody User user){

        ps.addUser(user.getAge(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getLevel(), user.getPassword(), user.getUsername());

        return "ok";
    }

    @RequestMapping("/find/name")
    public List<User> findByName(@RequestParam("first_name") String first_name){
        return ps.findByName(first_name);
    }

    @RequestMapping("/delete")
    public void deleteUser(@RequestParam("id") long id){
        ps.deleteUser(id);
    }

    @RequestMapping("/modify")
    public void modifyUser(@RequestParam("first_name") String first_name,
                             @RequestParam("id") long id){

        ps.modifyUser(first_name,id);
    }
}
