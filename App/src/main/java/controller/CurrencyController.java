package controller;

import model.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import service.CurrencyService;

import java.util.List;


@RestController
@RequestMapping("/search")
@CrossOrigin("/*")
public class CurrencyController {

    @Autowired
    CurrencyService search;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value="/search-id", produces = MediaType.APPLICATION_JSON_VALUE)
    public Currency searchCryptos(@RequestParam("id") String id){
        Currency currency = search.findById(id);
        return currency;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value="/top-10", produces = MediaType.APPLICATION_JSON_VALUE)
    public Currency[] searchTop10(){
        Currency[] currencies = search.findTop10();
        return currencies;
    }
}
