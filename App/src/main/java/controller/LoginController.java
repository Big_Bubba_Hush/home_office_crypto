package controller;


import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.LoginService;
import service.UserService;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    LoginService ls;

    @Autowired
    UserService ps;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value="/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public User login(@RequestParam("username") String first, @RequestParam("password") String last) {
        User result = ls.authenticate(first, last);
        return result;
    }

}
