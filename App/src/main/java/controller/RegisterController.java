package controller;

import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.UserService;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    UserService ps;

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add")
    public String addPerson(@RequestBody User user){
        System.out.println("HELLO");
        ps.addUser(user.getAge(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getLevel(), user.getPassword(), user.getUsername());

        return "ok";
    }

    @RequestMapping("*")
    public ModelAndView defaultPage() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("register");
        return mv;
    }
}
